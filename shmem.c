#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/fcntl.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define KEY         42
#define PAGE_SIZE   512
#define SHMEM_SIZE  (sizeof(ssize_t) + PAGE_SIZE)
#define MAXOPS      10

#define PTRERR      ((void*)(-1))

enum semnum_t {
    PRODUCER_POOL,
    CONSUMER_POOL,
    PRODUCER_PREV,
    CONSUMER_PREV,
    MUTEX,
    DATA,
    NSEMS
};

int producer(void *shm_p, int sem_id, const char *filename);
int consumer(void *shm_p, int sem_id);

int main(int argc, const char *argv[])
{
    if(!(argc == 2 || argc == 1)) {
        errno = EINVAL;
        perror("Program args");
        exit(EXIT_FAILURE);
    }

    int     sem_id  = semget(KEY, NSEMS, IPC_CREAT | 0666);
    if(sem_id < 0) {
        perror("semget");
        exit(EXIT_FAILURE);
    }

    int     shm_id  = shmget(KEY, SHMEM_SIZE, IPC_CREAT | 0666);
    if(shm_id < 0) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    void    *shm_p  = shmat(shm_id, NULL, 0);
    if(shm_p == PTRERR) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }
    if(argc == 2)
        producer(shm_p, sem_id, argv[1]);
    else
        consumer(shm_p, sem_id);
    return 0;
}

#define semop_push(which, semop, flag)                                      \
    do {                                                                    \
        semopbuf[semopidx].sem_num  = which;                                \
        semopbuf[semopidx].sem_op   = semop;                                \
        semopbuf[semopidx].sem_flg  = flag;                                 \
        semopidx++;                                                         \
    } while(0)

#define semop_flush(...) ({                                                 \
        int     _semopidx   = semopidx;                                     \
        semopidx            = 0;                                            \
        int     _res        = semop(sem_id, semopbuf, _semopidx);           \
        _res;                                                               \
    })

int producer(void *shm_p, int sem_id, const char *filename)
{
    struct  sembuf  semopbuf[MAXOPS]    = {};
    int     semopidx                    = 0;

    int     src     = open(filename, O_RDONLY);
    if(src < 0) {
        perror("src file open");
        exit(EXIT_FAILURE);
    }

    semop_push(PRODUCER_POOL, 0, 0);
    semop_push(PRODUCER_POOL, 1, SEM_UNDO);
    semop_push(CONSUMER_PREV, 0, 0);
    semop_flush();

    semop_push(CONSUMER_POOL, -1, 0);
    semop_push(CONSUMER_POOL,  1, 0);
    semop_push(PRODUCER_PREV,  1, SEM_UNDO);
    semop_flush();

    semop_push(DATA,  1, SEM_UNDO);
    semop_push(DATA, -1, 0);
    semop_flush();

    ssize_t it_read = 0;
    do {
        semop_push(CONSUMER_POOL, -1, IPC_NOWAIT);
        semop_push(CONSUMER_POOL,  1, 0);
        semop_push(DATA,           0, 0);
        semop_push(MUTEX,          0, 0);
        semop_push(MUTEX,          1, SEM_UNDO);
        if(semop_flush() == -1) {
            perror("Consumer");
            exit(EXIT_FAILURE);
        }

        it_read = read(src, shm_p + sizeof(ssize_t), PAGE_SIZE);
        if(it_read < 0) {
            perror("src file read");
            exit(EXIT_FAILURE);
        }
        *((ssize_t*)shm_p) = it_read;

        semop_push(DATA,   1, 0);
        semop_push(MUTEX, -1, SEM_UNDO);
        semop_flush();
    } while(it_read > 0);

    exit(EXIT_SUCCESS);

    return 0;
}

int consumer(void *shm_p, int sem_id)
{
    struct  sembuf  semopbuf[MAXOPS]    = {};
    int     semopidx                    = 0;

    semop_push(CONSUMER_POOL, 0, 0);
    semop_push(CONSUMER_POOL, 1, SEM_UNDO);
    semop_push(PRODUCER_PREV, 0, 0);
    semop_flush();

    semop_push(PRODUCER_POOL, -1, 0);
    semop_push(PRODUCER_POOL,  1, 0);
    semop_push(CONSUMER_PREV,  1, SEM_UNDO);
    semop_flush();

    ssize_t it_read = 0;
    do {
        semop_push(PRODUCER_POOL, -1, IPC_NOWAIT);
        semop_push(PRODUCER_POOL,  1, 0);
        semop_push(DATA,          -1, 0);
        semop_push(MUTEX,          0, 0);
        semop_push(MUTEX,          1, SEM_UNDO);
        if(semop_flush() == -1) {
            perror("Producer");
            exit(EXIT_FAILURE);
        }

        it_read = *((ssize_t*)shm_p);
        write(STDOUT_FILENO, shm_p + sizeof(ssize_t), it_read);

        semop_push(MUTEX, -1, SEM_UNDO);
        semop_flush();
    } while(it_read > 0);

    exit(EXIT_SUCCESS);

    return 0;
}

